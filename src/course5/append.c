#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>

int main(void)
{
    int fd = open("test_open.txt", O_RDWR | O_APPEND);
    if (fd < 0)
    {
        perror("open");
    }

    int pos = lseek(fd, 2, SEEK_SET);
    char buf[10];
    read(fd, buf, 1);
    printf("%s", buf);
    if (pos != 0)
    {
        // printf("lseek %d\n", pos);
    }

    int size = write(fd, "world", strlen("world"));
    if (size != 5)
    {
        printf("write error\n");
    }

    pos = lseek(fd, -1, SEEK_CUR);
    read(fd, buf, 1);
    printf("%s", buf);

    close(fd);
    return 0;
}