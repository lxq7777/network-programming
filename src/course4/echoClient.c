/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-22 11:51:55
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-22 12:31:24
 */
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ECHOMAX 255

int main(int argc, char *argv[])
{
    // socket 描述符
    int sock;
    // server 地址
    struct sockaddr_in echoServAddr;
    // client 地址
    struct sockaddr_in fromAddr;
    // server 端口
    unsigned short echoServPort;

    unsigned int fromSize;

    // server ip
    char *servIP;
    // 发送字符
    char *echoString;
    // 接收字符
    char echoBuffer[ECHOMAX + 1];

    // 发送字符长度
    int echoStringlen;
    // 接收字符长度
    int respStringlen;

    if ((argc < 3) || (argc > 4))
    {
        printf("Usage: %s <Server IP> <Echo Word> [<Echo Port>]\n",
               argv[0]);
        exit(1);
    }

    // 映射参数
    servIP = argv[1];
    echoString = argv[2];

    // 参数校验：端口号，字符长度
    if ((echoStringlen = strlen(echoString)) > ECHOMAX)
    {
        printf("Echo word too long.\n");
    }

    if (argc == 4)
    {
        echoServPort = atoi(argv[3]);
    }
    else
    {
        // echoServer 预留端口
        echoServPort = 7;
    }

    // 创建socket
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        printf("socket() failed. \n");
    }

    // 分配空间
    memset(&echoServAddr, 0, sizeof(echoServAddr));

    // 一个协议簇只对应一种地址簇，因此PF_INET = AF_INET
    echoServAddr.sin_family = AF_INET;
    // server ip
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);
    echoServAddr.sin_port = htons(echoServPort);

    // 通过socket 发送对应的数据包
    if ((sendto(sock, echoString, echoStringlen, 0, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr))) != echoStringlen)
    {
        printf("sendto() sent a different number of bytes than expected.\n");
    }

    fromSize = sizeof(fromAddr);

    // 接受数据
    if ((respStringlen = recvfrom(sock, echoBuffer, ECHOMAX, 0, (struct sockaddr *)&fromAddr, &fromSize)) != echoStringlen)
    {
        printf("recvfrom() failed\n");
    }

    // 识别数据来源
    if (echoServAddr.sin_addr.s_addr != fromAddr.sin_addr.s_addr)
    {
        printf("Error: received a packet from unknown source.\n");
        exit(1);
    }
    
    echoBuffer[respStringlen] = '\0';
    printf("Received: %s\n",echoBuffer);
    close(sock);
    exit(0);
}