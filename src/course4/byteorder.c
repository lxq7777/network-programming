/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-22 10:26:55
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-22 10:26:56
 */
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
int main(void)
{
    union
    {
        short s;
        char c[sizeof(short)];
    } un;
    un.s = 0x0102;
    if (sizeof(short) == 2)
    {
        if (un.c[0] == 1 && un.c[1] == 2)
            printf("big-endian\n");
        else if (un.c[0] == 2 && un.c[1] == 1)
            printf("littlt-endian\n");
        else
            printf("Unknow\n");
    }
    else
        printf("sizeof(short)=%d\n", sizeof(short));
    exit(0);
}