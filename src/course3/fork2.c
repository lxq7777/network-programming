/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-08 10:50:19
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-08 11:49:20
 */
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

// 注：子进程创建不了请提高权限
int main(void)
{
    pid_t t;
    printf("Original program, pid=%d\n", getpid());
    t = fork();
    if (t == 0)
    {
        printf("In child process, pid=%d, ppid=%d\n",
               getpid(), getppid());
    }
    else
    {
        printf("In parent, pid=%d, fork returned=%d\n",
               getpid(), t);
    }
}