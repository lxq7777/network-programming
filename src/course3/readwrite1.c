/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-08 11:24:57
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-08 11:48:12
 */

#include <fcntl.h>
#include <unistd.h>

int main(void)
{
    // 退出输入 .
    char quit = '.';
    char buf[10];
    int fd;
    if ((fd = open("out.out", O_RDWR | O_CREAT, 0)) == -1)
        printf("Error in opening\n");
    while (buf[0] != quit)
    {
        // 从命令行读入
        read(0, buf, 1);
        // 写入到文件
        write(fd, buf, 1);
        // 输出到命令行
        write(1, buf, 1);
    }
    close(fd);
}
