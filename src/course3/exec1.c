/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-08 10:54:14
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-08 11:34:03
 */
#include <unistd.h>
#include <stdio.h>

int main(void)
{
    // 指针数组类型
    char *arg[] = {"/bin/ls", 0};
    /* fork, and exec within child process */
    if (fork() == 0)
    {
        // 在子进程之中
        printf("In child process:\n");
        // 子进程执行ls
        execv(arg[0], arg);
        printf("I will never be called\n");
    }
    printf("Execution continues in parent process\n");
}