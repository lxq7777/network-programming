/*
 * @version: 1.0
 * @Author: Yibai Jiang
 * @Date: 2022-03-08 10:29:23
 * @LastEditors: Yibai Jiang
 * @LastEditTime: 2022-03-08 11:33:59
 */
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main (){
    pid_t t;
    // fork 相当于产生了两个一模一样的进程 均执行后续代码
    t = fork();
    printf("fork returned %d\n", t);
    exit(0);
}